package exercises

import matryoshka._
import matryoshka.data.Fix
import matryoshka.implicits._
import slamdata.Predef._

import scalaz.Functor

object Exercise extends App {

  sealed trait FileSystem[A]
  final case class Folder[A](size: Int, left: A, right: A) extends FileSystem[A]
  final case class File[A](size: Int)                      extends FileSystem[A]
  final case class FolderEmpty[A]()                        extends FileSystem[A]

  implicit val fileF: Functor[FileSystem] = { ??? }

  val fileSystem: Fix[FileSystem] = ???

  val coAlgebra = { ??? }

  val algebra = { ??? }

  ??? //тест coAlgebra

  ??? //тест algebra
}
